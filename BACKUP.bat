@echo off

set "prefix="

FOR %%a IN (.) DO SET "current=%%~nxa"

SET "PROJ_NAME=%prefix%%current%"

SET "SevenZip=%ProgramFiles%\7-zip\7z.exe"
SET "BKP_HOME=I:\!BACKUP\test-items"
SET "BKP_WORK=N:\!BACKUP"

:work
if not exist C:\XEClient goto home
set "BKP_DIR=%BKP_WORK%"
goto skip_home

:home
if not exist %BKP_HOME% exit
set "BKP_DIR=%BKP_HOME%"
:skip_home

@echo off
set now=%TIME:~0,-3%
set now=%now::=.%
set now=%now: =0%
set now=%DATE:~-4%.%DATE:~3,2%.%DATE:~0,2%_%now%

@echo on
"%SevenZip%" a -t7z -m0=lzma2 -mmt4 -mx9 "%BKP_DIR%\%PROJ_NAME%_%now%.7z" %~dp0* -xr!node_modules -xr!dist -xr!vendor
:end

