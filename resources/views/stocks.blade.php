<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Stocks</title>
        <!-- <link href="/css/slim.min.css" rel="stylesheet"> -->
    </head>
    <body>
		<div id="app">
		<!-- <p>Stocks</p> -->
		</div>
    </body>
    <script src="/js/app.js" type="text/javascript"></script>
</html>
