# stocks

## Project setup
```
composer install
yarn
```

### Compiles and hot-reloads for development
```
yarn run dev
```

### Compiles and minifies for production
```
yarn run prod
```
