<?php

namespace App\Stocks;

class Store
{
    private const URL = 'http://phisix-api3.appspot.com/stocks.json';

    public function getStocks(): array
    {
        $rawJson = file_get_contents(self::URL);
        $data = json_decode($rawJson, true);

        return array_map(function($row){
            return [
                'name' => $row['name'],
                'amount' => number_format($row['price']['amount'], 2),
                'volume' => $row['volume']
            ];
        }, $data['stock']);
    }
}
